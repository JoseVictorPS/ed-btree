#include "btree.h"

//gcc btree.c main.c -o btree

int main(int argc, char *argv[]){
  BTree * arvore = initialize();
  int num = 0, from, to;
  readT();
  while(num != -1){
    printf("Digite um numero para adicionar. 0 para imprimir. -9 para remover e -1 para sair\n");
    scanf("%i", &num);
    if(num == -9){
      scanf("%d", &from);
      arvore = retira(arvore, from);
      printTree(arvore);
    }
    else if(num == -1){
      printf("\n");
      printTree(arvore);
      freeBTree(arvore);
      return 0;
    }
    else if(!num){
      printf("\n");
      printTree(arvore);
    }
    else arvore = inserts(arvore, num);
    printf("\n\n");
  }
}
