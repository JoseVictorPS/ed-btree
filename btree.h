#ifndef BTREE_H
#define BTREE_H

#include <stdio.h>
#include <stdlib.h>

typedef struct btree{
  int activKeys, isLeaf, *keys;
  struct btree **children;
}BTree;

//Read value to "t"
void readT();
//Allocates a BTree
BTree *create();
//Frees the BTree
BTree *freeBTree(BTree *a);
//Prints the BTreee
void printTree(BTree*a);
//Searchs for a key
BTree *search(BTree* x, int key);
//Initializes pointing to null
BTree *initialize();
/*Inserts in empty trees, manages root full of keys
* or call auxiliar functions*/
BTree *inserts(BTree *T, int k);
/*Checks if the key isn't in the tree, or if the key is empty
* otherwise, calls auxiliar function*/
BTree* retira(BTree* arv, int k);


#endif //BTREE_H
