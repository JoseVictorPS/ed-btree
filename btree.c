#include <stdio.h>
#include <stdlib.h>

int t;

#include "btree.h"

//Actual removal
static BTree* remover(BTree* arv, int ch);
//Printing with recursion
static void printsTree(BTree *a, int depth);
//Splits the node updating the parent(does not insert any key)
static BTree *spliting(BTree *p, BTree* c, int n);
//Insertion in children or, if leaf, in the "x" node
static BTree *insertNotFull(BTree *x, int k);

/* (OBS 1) "i" as index for both keys and children:
* if "i" is index for a key "k", then it will also be
* the index for the last child that has the smaller than "k" elements*/

/* Iteration through the activeKeys:
* when i < activeKeys, the index "i" iterates through the entire array of keys
* when i <= activeKeys, the index "i" iterates through the entire array of children
* activKeys: index of last child; activKeys-1: index of last key*/

void readT() {
  printf("Digite um valor para t:  ");
  scanf("%d", &t);
}

BTree *create(){
  //Integer constant "t"
  //Allocates new structure for BTree called "newT"
  BTree* newT = (BTree*)malloc(sizeof(BTree));
  //Number of active keys is 0
  newT->activKeys = 0;
  //Array of integer keys is allocated
  newT->keys =(int*)malloc(sizeof(int*)*((t*2)-1));
  //Boolean for leaf is assigned as true
  newT->isLeaf=1;
  //Allocate array of pointers to children
  newT->children = (BTree**)malloc(sizeof(BTree*)*t*2);
  int i;
  //Fills the array of pointer to children with null
  for(i=0; i<(t*2); i++) newT->children[i] = NULL;
  //Returns the structure "newT"
  return newT;
}


BTree *freeBTree(BTree *a){
  //The B tree to be freed "a"
  //If the pointer is valid
  if(a){
    //If is not a leaf
    if(!a->isLeaf){
      int i;
      //Calls recursion to it's children
      for(i = 0; i <= a->activKeys; i++) freeBTree(a->children[i]);
    }
    //Frees the array of integer keys
    free(a->keys);
    //Frees the array of pointers to children
    free(a->children);
    //Frees the current node
    free(a);
    //Updates the calling pointer to null
    return NULL;
  }
}

void printTree(BTree*a) {
  //The B tree to be printed "a"
  //Calls auxiliar function with depth "0"
  printsTree(a, 0);
}

static void printsTree(BTree *a, int depth){
  //The B tree to be printed "a"; the depth we're printing "depth"
  //If the pointer is valid
  if(a){
    int i,j;
    //Iterates through the index of keys
    for(i=0; i<=a->activKeys-1; i++){
      //Calls recursion to each of it's children with a increased depth
      printsTree(a->children[i],depth+1);
      //Iterates through the amount of depth adding 3 blankspaces each
      for(j=0; j<=depth; j++) printf("   ");
      //Prints the position i in the array keys
      printf("%d\n", a->keys[i]);
    }
    //Calls recursion to it's last children
    printsTree(a->children[i],depth+1);
  }
}

BTree *search(BTree* x, int key){
  //The B tree we're searching "x"; The key we're searching "key"
  //Initializes pointer of BTree to null
  BTree *notFound = NULL;
  //If the tree is empty
  if(!x) return notFound;
  int i = 0;
  //Searches for the index of where the key should be
  while(i < x->activKeys && key > x->keys[i]) i++;
  //If the while above stopped because of the second condition
  if(i < x->activKeys && key == x->keys[i]) return x;
  //If node x is leaf
  if(x->isLeaf) return notFound;
  //Returns recursion to it's children on index i
  return search(x->children[i], key);
}


BTree *initialize(){
  //Returns null to the calling pointer
  return NULL;
}

BTree *inserts(BTree *T, int k){
  //If the information is already on the tree, returns the tree inaltered
  if(search(T,k)) return T;
  //If the tree is empty
  if(!T){
    //Creates a empty node for a btree
    T=create();
    //The first key is set to "k"
    T->keys[0] = k;
    //Number of active keys is set to 1
    T->activKeys=1;
    //Returns the node
    return T;
  }
  //If the array of keys is full
  if(T->activKeys == (2*t)-1){
    //Creates a empty node to be the new root
    BTree *newRoot = create();
    //Sets boolean for leaf to false
    newRoot->isLeaf = 0;
    //Sets the first child to the previus root
    newRoot->children[0] = T;
    //Splits the old root bringing up one key to the new root
    newRoot = spliting(newRoot, T, 1);
    //Inserts the element in children of the new root
    newRoot = insertNotFull(newRoot, k);
    //Returns the new root
    return newRoot;
  }
  //If the array is not full
  T = insertNotFull(T,k);
  //Returns the root
  return T;
}

static BTree *spliting(BTree *p, BTree* c, int n){
  /*The parent node "p"; the child node "c"; 
  * the index of the child right after "c"; the constant "t"*/
  /*the node "c" will hold the smaller keys, and
  * the node "s" will hold the bigger ones*/
  //Creates the node to be paired with the child "c", the Sibling "s"
  BTree *s=create();
  /* When the node with (2*t)-1 keys splits, 1 key goes up, so...
  * ((2*t)-1)-1 = (2*t)-2 = 2*(t-1)
  * When spliting, the node is divided in half, so...
  * (2*(t-1))/2 = t-1 */ 
  s->activKeys= t - 1;
  //"s" has to be in the same depth as "c", so...
  s->isLeaf = c->isLeaf;
  int j;
  //Assigns the last elements on the child to it's sibling
  //From the first element after the half to the last key
  for(j=0; j < s->activKeys; j++) s->keys[j] = c->keys[j+t];
  //If the child has children
  if(!c->isLeaf){
    //Gives the last children of "c" to "s"
    //From the first child after the half to the last
    for(j=0; j<t; j++){
      s->children[j] = c->children[j+t];
      c->children[j+t] = NULL;
    }
  }
  /*Child will have the same amount of keys as the sibling
  * For the same reasons*/
  c->activKeys = t-1;
  //Pushes the children foward
  //From the first child after "c" to the last
  for(j=p->activKeys; j>=n; j--) p->children[j+1]=p->children[j];
  //Assigns the first child after "c" to be the sibling "s"
  p->children[n] = s;
  //Pushes the keys foward
  //From the first key
  for(j=p->activKeys; j>=n; j--) p->keys[j] = p->keys[j-1];
  //The key in the middle of the child "c",
  /*goes up to the index where holds the key
  * bigger than the elements in the child "c" (OBS 1) */
  p->keys[n-1] = c->keys[t-1];
  //The key that went up
  p->activKeys++;
  //Returns the updated parent
  return p;
}


static BTree *insertNotFull(BTree *x, int k){
  //The B tree we're inserting "x"; The key we're inserting "key"; The constant "t"
  //Assigns i to last index of keys
  int i = x->activKeys-1;
  //If node is a leaf (Insertion on the node)
  if(x->isLeaf){
    //Searchs the index of the last element smaller than k
    while((i>=0) && (k<x->keys[i])){
      //Pushes foward the elements
      x->keys[i+1] = x->keys[i];
      //Decrements i
      i--;
    }
    //Assigns k to the right position
    x->keys[i+1] = k;
    //Increments number of active keys
    x->activKeys++;
    //Returns the node
    return x;
  }
  //If node isn't a leaf (Insertion on it's child)
  //Searchs the index of the last element smaller than k
  while((i>=0) && (k<x->keys[i])) i--;
  //Assigns i to child supposed to insert
  i++;
  //If the child where k will be inserted is full
  if(x->children[i]->activKeys == ((2*t)-1)){
    //splits the child node updating the current node
    x = spliting(x, x->children[i], (i+1));
    //If the key that came up is smaller than k
    if(k>x->keys[i]) i++;
  }
  //Inserts on the child "i", or it's children
  x->children[i] = insertNotFull(x->children[i], k);
  //Returns the node x
  return x;
}

BTree* retira(BTree* arv, int k){
  if(!arv || !search(arv, k)) return arv;
  return remover(arv, k);
}

static BTree* remover(BTree* arv, int ch){
  if(!arv) return arv;
  int i;
  printf("Removendo %d...\n", ch);
  for(i = 0; i<arv->activKeys && arv->keys[i] < ch; i++);
  if(i < arv->activKeys && ch == arv->keys[i]){ //CASOS 1, 2A, 2B e 2C
    if(arv->isLeaf){ //CASO 1
      printf("\nCASO 1\n");
      int j;
      for(j=i; j<arv->activKeys-1;j++) arv->keys[j] = arv->keys[j+1];
      arv->activKeys--;
      return arv;      
    }
    if(!arv->isLeaf && arv->children[i]->activKeys >= t){ //CASO 2A
      printf("\nCASO 2A\n");
      BTree *y = arv->children[i];  //Encontrar o predecessor k' de k na árvore com raiz em y
      while(!y->isLeaf) y = y->children[y->activKeys];
      int temp = y->keys[y->activKeys-1];
      arv->children[i] = remover(arv->children[i], temp); 
      //Eliminar recursivamente K e substitua K por K' em x
      arv->keys[i] = temp;
      return arv;
    }
    if(!arv->isLeaf && arv->children[i+1]->activKeys >= t){ //CASO 2B
      printf("\nCASO 2B\n");
      BTree *y = arv->children[i+1];  //Encontrar o sucessor k' de k na árvore com raiz em y
      while(!y->isLeaf) y = y->children[0];
      int temp = y->keys[0];
      y = remover(arv->children[i+1], temp); //Eliminar recursivamente K e substitua K por K' em x
      arv->keys[i] = temp;
      return arv;
    }
    if(!arv->isLeaf && arv->children[i+1]->activKeys == t-1 && arv->children[i]->activKeys == t-1){ //CASO 2C
      printf("\nCASO 2C\n");
      BTree *y = arv->children[i];
      BTree *z = arv->children[i+1];
      y->keys[y->activKeys] = ch;          //colocar ch ao final de children[i]
      int j;
      for(j=0; j<t-1; j++)                //juntar keys[i+1] com keys[i]
        y->keys[t+j] = z->keys[j];
      for(j=0; j<=t; j++)                 //juntar children[i+1] com children[i]
        y->children[t+j] = z->children[j];
      y->activKeys = 2*t-1;
      for(j=i; j < arv->activKeys-1; j++)   //remover ch de arv
        arv->keys[j] = arv->keys[j+1];
      for(j=i+1; j <= arv->activKeys; j++)  //remover ponteiro para children[i+1]
        arv->children[j] = arv->children[j+1];
      arv->children[j] = NULL; //Campello
      arv->activKeys--;
      arv->children[i] = remover(arv->children[i], ch);
      return arv;   
    }   
  }

  BTree *y = arv->children[i], *z = NULL;
  if (y->activKeys == t-1){ //CASOS 3A e 3B
    if((i < arv->activKeys) && (arv->children[i+1]->activKeys >=t)){ //CASO 3A
      printf("\nCASO 3A: i menor que activKeys\n");
      z = arv->children[i+1];
      y->keys[t-1] = arv->keys[i];   //dar a y a keys i da arv
      y->activKeys++;
      arv->keys[i] = z->keys[0];     //dar a arv uma keys de z
      int j;
      for(j=0; j < z->activKeys-1; j++)  //ajustar keyss de z
        z->keys[j] = z->keys[j+1];
      //z->keys[j] = 0; Rosseti
      y->children[y->activKeys] = z->children[0]; //enviar ponteiro menor de z para o novo elemento em y
      for(j=0; j < z->activKeys; j++)       //ajustar childrens de z
        z->children[j] = z->children[j+1];
      z->activKeys--;
      arv->children[i] = remover(arv->children[i], ch);
      return arv;
    }
    if((i > 0) && (!z) && (arv->children[i-1]->activKeys >=t)){ //CASO 3A
      printf("\nCASO 3A: i igual a activKeys\n");
      z = arv->children[i-1];
      int j;
      for(j = y->activKeys; j>0; j--)               //encaixar lugar da nova keys
        y->keys[j] = y->keys[j-1];
      for(j = y->activKeys+1; j>0; j--)             //encaixar lugar dos childrens da nova keys
        y->children[j] = y->children[j-1];
      y->keys[0] = arv->keys[i-1];              //dar a y a keys i da arv
      y->activKeys++;
      arv->keys[i-1] = z->keys[z->activKeys-1];   //dar a arv uma keys de z
      y->children[0] = z->children[z->activKeys];         //enviar ponteiro de z para o novo elemento em y
      z->activKeys--;
      arv->children[i] = remover(y, ch);
      return arv;
    }
    if(!z){ //CASO 3B
      if(i < arv->activKeys && arv->children[i+1]->activKeys == t-1){
        printf("\nCASO 3B: i menor que activKeys\n");
        z = arv->children[i+1];
        y->keys[t-1] = arv->keys[i];     //pegar keys [i] e coloca ao final de children[i]
        y->activKeys++;
        int j;
        for(j=0; j < t-1; j++){
          y->keys[t+j] = z->keys[j];     //passar children[i+1] para children[i]
          y->activKeys++;
        }
        if(!y->isLeaf){
          for(j=0; j<t; j++){
            y->children[t+j] = z->children[j];
          }
        }
        for(j=i; j < arv->activKeys-1; j++){ //limpar referências de i
          arv->keys[j] = arv->keys[j+1];
          arv->children[j+1] = arv->children[j+2];
        }
        arv->activKeys--;
        arv = remover(arv, ch);
        return arv;
      }
      if((i > 0) && (arv->children[i-1]->activKeys == t-1)){ 
        printf("\nCASO 3B: i igual a activKeys\n");
        z = arv->children[i-1];
        if(i == arv->activKeys)
          z->keys[t-1] = arv->keys[i-1]; //pegar keys[i] e poe ao final de children[i-1]
        else
          z->keys[t-1] = arv->keys[i];   //pegar keys [i] e poe ao final de children[i-1]
        z->activKeys++;
        int j;
        for(j=0; j < t-1; j++){
          z->keys[t+j] = y->keys[j];     //passar children[i+1] para children[i]
          z->activKeys++;
        }
        if(!z->isLeaf){
          for(j=0; j<t; j++){
            z->children[t+j] = y->children[j];
          }
        }
        arv->activKeys--;
        arv->children[i-1] = z;
        arv = remover(arv, ch);
        return arv;
      }
    }
  }  
  arv->children[i] = remover(arv->children[i], ch);
  return arv;
}